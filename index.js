const readLine = require('readline'); 
const fileServer = require('fs');
const os = require('os');
const http = require('http');

function createInputInterface() {
    let inputInterface = readLine.createInterface({
    input: process.stdin,
    output: process.stdout
    });

    return inputInterface;
}

function readAndDisplayFile() {
    fileServer.readFile(__dirname + '/package.json', 'utf-8', (error, content) => {
        console.log(content);
    })
}

function displayOsInfo() {
    let output = 
        `Getting OS info:\nSYSTEM MEMORY: ${(os.totalmem()/1024/1024/1024).toFixed(2)}GB\n
        FREE MEMORY: ${(os.freemem()/1024/1024/1024).toFixed(2)}GB\n
        CPU CORES: ${os.cpus().length}\n
        ARCH: ${os.arch()}\n
        PLATFORM: ${os.platform()}\n
        RELEASE: ${os.release()}\n
        USER: ${os.userInfo().uid}`;
    
    console.log(output);
}

function startHttpServer(port = 3000) {
    const server = http.createServer((req, res) => {
        res.statusCode = 200;
        res.setHeader('Content-Type', 'text/plain');
        res.end('Hello World!');
    });

    server.listen(port, function() {
        console.log(`Starting server at localhost:${port}`);
    })
}

function createInterfaceForInput() {
    createInputInterface().question('Choose and option:\n1. Read package.json\n2. Display OS info\n3. Start HTTP server\nType a number: ', function (answer) {
        switch (answer) {
            case "1":
                readAndDisplayFile();
                break;
            case "2":
                displayOsInfo();
                break;
            case "3":
                startHttpServer();
                break;
            default:
                console.log("Wrong input. Try again");
                break;
        }
    });
}

createInterfaceForInput();